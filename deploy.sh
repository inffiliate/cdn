if [ -z "$1" ]; then
    echo "Deployment failed!"
    echo "Deployment environment is required (dev/prod)"
    exit 1
elif [[ "$1" == "dev" ]]; then
    path="/dev/*"
elif [[ "$1" == "prod" ]]; then
    path="/*"
else
    echo "Deployment failed!"
    echo "Invalid environment (dev/prod)"
    exit 1
fi

echo 'Syncing S3 bucket'
aws s3 sync . s3://cdn.inffiliate.com \
    --exclude ".git/*" \
    --exclude "deploy.sh"

echo 'Invalidating Cloudfront cache'

AWS_PAGER="" aws cloudfront create-invalidation --distribution-id=E3USC833YXN8GO --paths $path

echo 'Deployment completed'
