class Inffiliate {
  constructor(integration, shop) {
    this.integration = integration;
    this.shop = shop;

    this.host = `https://dev.inffiliate.com/apps/${integration}/api`;

    this.headers = {
      "Content-Type": "application/json",
      "X-Inffiliate-Shop-Domain": shop,
    };
  }

  trackVisit() {
    const clickId = new URLSearchParams(window.location.search).get("click_id");

    if (clickId) this.setCookie("infl_click_id", clickId, 30);
  }

  async trackOrder(params) {
    try {
      await fetch(this.host + "/orders", {
        method: "POST",
        headers: this.headers,
        body: JSON.stringify({
          order_id: params.orderId,
          order_number: params.orderNumber,
          total_price: params.totalPrice,
          subtotal_price: params.subtotalPrice,
          discount_amount: params.discountAmount,
          click_id: this.getCookie("infl_click_id"),
        }),
      });

      this.deleteCookie("infl_click_id");
    } catch (e) {
      console.error(e);
    }
  }

  setCookie(name, value, days) {
    var expires;

    if (days) {
      var date = new Date();
      date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
      expires = "; expires=" + date.toGMTString();
    } else {
      expires = "";
    }
    document.cookie =
      encodeURIComponent(name) +
      "=" +
      encodeURIComponent(value) +
      expires +
      "; path=/";
  }

  getCookie(name) {
    var nameEQ = encodeURIComponent(name) + "=";
    var ca = document.cookie.split(";");
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) === " ") c = c.substring(1, c.length);
      if (c.indexOf(nameEQ) === 0)
        return decodeURIComponent(c.substring(nameEQ.length, c.length));
    }
    return null;
  }

  deleteCookie(name) {
    this.setCookie(name, "", -1);
  }
}

window.Inffiliate = Inffiliate;
