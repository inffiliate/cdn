(async function () {
  let clickId = new URLSearchParams(window.location.search).get("click_id");

  if (clickId) {
    setCookie("inf_click_id", clickId, 30);
  }

  if (
    Shopify.Checkout &&
    (Shopify.Checkout.page == "thank_you" || Shopify.Checkout.isOrderStatusPage)
  ) {
    clickId = getCookie("inf_click_id");

    if (!clickId) return;

    const checkout = Shopify.checkout;

    try {
      await fetch("https://api.inffiliate.com/plugins/shopify/orders", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "X-Inffiliate-Shop-Domain": Shopify.shop,
        },
        body: JSON.stringify({
          order_id: checkout.order_id,
          // order_number: params.orderNumber,
          // total_price: params.totalPrice,
          // subtotal_price: params.subtotalPrice,
          // discount_amount: params.discountAmount,
          click_id: clickId,
        }),
      });

      deleteCookie("inf_click_id");
    } catch (e) {
      console.error(e);
    }
  }

  function setCookie(name, value, days) {
    var expires;

    if (days) {
      var date = new Date();
      date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
      expires = "; expires=" + date.toGMTString();
    } else {
      expires = "";
    }
    document.cookie =
      encodeURIComponent(name) +
      "=" +
      encodeURIComponent(value) +
      expires +
      "; path=/";
  }

  function getCookie(name) {
    var nameEQ = encodeURIComponent(name) + "=";
    var ca = document.cookie.split(";");
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) === " ") c = c.substring(1, c.length);
      if (c.indexOf(nameEQ) === 0)
        return decodeURIComponent(c.substring(nameEQ.length, c.length));
    }
    return null;
  }

  function deleteCookie(name) {
    setCookie(name, "", -1);
  }
})();
