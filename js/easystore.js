(async function () {
  const shop = new URLSearchParams(
    new URL(document.currentScript.src).search
  ).get("shop");

  let clickId = new URLSearchParams(window.location.search).get("click_id");

  if (clickId) {
    setCookie("inf_click_id", clickId, 30);
  }

  const template = window.__st.p;

  if (template == "product") {
    // document.querySelector(".btn.addToCart-btn").click();
  }

  if (template == "payment_completed") {
    clickId = getCookie("inf_click_id");

    if (!clickId) return;

    const order = $("[data-app-purchase]").data("app-purchase");

    try {
      await fetch("https://api.inffiliate.com/plugins/easystore/orders", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "X-Inffiliate-Shop-Domain": shop,
        },
        body: JSON.stringify({
          order_id: order.id,
          // order_number: params.orderNumber,
          // total_price: params.totalPrice,
          // subtotal_price: params.subtotalPrice,
          // discount_amount: params.discountAmount,
          click_id: clickId,
        }),
      });

      deleteCookie("inf_click_id");
    } catch (e) {
      console.error(e);
    }
  }

  function setCookie(name, value, days) {
    var expires;

    if (days) {
      var date = new Date();
      date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
      expires = "; expires=" + date.toGMTString();
    } else {
      expires = "";
    }
    document.cookie =
      encodeURIComponent(name) +
      "=" +
      encodeURIComponent(value) +
      expires +
      "; path=/";
  }

  function getCookie(name) {
    var nameEQ = encodeURIComponent(name) + "=";
    var ca = document.cookie.split(";");
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) === " ") c = c.substring(1, c.length);
      if (c.indexOf(nameEQ) === 0)
        return decodeURIComponent(c.substring(nameEQ.length, c.length));
    }
    return null;
  }

  function deleteCookie(name) {
    setCookie(name, "", -1);
  }
})();
